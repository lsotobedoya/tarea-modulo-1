import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaBanderasComponent } from './lista-banderas.component';

describe('ListaBanderasComponent', () => {
  let component: ListaBanderasComponent;
  let fixture: ComponentFixture<ListaBanderasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaBanderasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaBanderasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
