import { Component, OnInit } from '@angular/core';
import { Bandera } from '../models/bandera';

@Component({
  selector: 'app-lista-banderas',
  templateUrl: './lista-banderas.component.html',
  styleUrls: ['./lista-banderas.component.css'],
})
export class ListaBanderasComponent implements OnInit {
  titulo: string;
  descripcion: string;
  banderas: Bandera[];
  banderasFormulario: Bandera[];
  constructor() {
    this.titulo = 'Banderas del mundo';
    this.descripcion = 'Trabajo del módulo 1 del curso de Angular 2020';
    this.banderas = [];
    this.banderasFormulario = [];
  }

  ngOnInit() {
    this.cargarBanderasIniciales();
  }

  private cargarBanderasIniciales() {
    const colombia = new Bandera('flag co');
    const argentina = new Bandera('flag ar');
    const usa = new Bandera('flag us');
    this.banderas.push(colombia);
    this.banderas.push(argentina);
    this.banderas.push(usa);
  }

  guardar(valorBandera: string): boolean {
    this.banderasFormulario.push(new Bandera(valorBandera));
    return false;
  }
}
