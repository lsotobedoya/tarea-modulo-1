import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListaBanderasComponent } from './lista-banderas/lista-banderas.component';
import { BanderaComponent } from './bandera/bandera.component';

@NgModule({
  declarations: [
    AppComponent,
    ListaBanderasComponent,
    BanderaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
