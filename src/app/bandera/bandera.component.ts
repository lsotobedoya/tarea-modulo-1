import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { Bandera } from '../models/bandera';
import { Guid } from 'guid-typescript';

@Component({
  selector: 'app-bandera',
  templateUrl: './bandera.component.html',
  styleUrls: ['./bandera.component.css']
})
export class BanderaComponent implements OnInit {

  @Input() bandera: Bandera;

  @HostBinding('attr.id') id = Guid.create();

  constructor() { }

  ngOnInit() {
  }

}
